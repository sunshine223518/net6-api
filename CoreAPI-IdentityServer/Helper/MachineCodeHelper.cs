﻿using System.Diagnostics;
using System.IO;

namespace CoreAPI_IdentityServer.Helper
{
    /// <summary>
    /// 获取设备码
    /// </summary>
    public class MachineCodeHelper
    {
        public static string GetOnly()
        {
            string sPath, Str;
            sPath = Path.GetTempFileName();
            Str = string.Format("FOR /F \"tokens=3\" %I IN ('CSCRIPT %WINDIR%\\SYSTEM32\\SLMGR.VBS /DTI') DO ECHO %I > {0}", sPath);
            Process p = new();
            p.StartInfo.FileName = "CMD.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.StandardInput.WriteLine(Str);
            p.StandardInput.WriteLine("Exit");
            p.StandardInput.AutoFlush = true;
            p.WaitForExit();
            p.Close();
            Str = File.ReadAllText(sPath).TrimEnd().Replace("\r\n", "");
            return Str;
        }
    }
}
