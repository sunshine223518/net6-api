﻿using System;

namespace CoreAPI_IdentityServer.Models.ViewModel
{
    public class UserInfoSinilgePoints
    {
        public string UserName { get;  set; }
        public DateTime LastTime { get;  set; }
        public string OnlyCode { get;  set; }
        public string Ip { get;  set; }
    }
}
