using CoreAPI.IdentityServer;
using CoreAPI.IdentityServer.Authorization;
using CoreAPI.IdentityServer.Extensions;
using CoreAPI.IdentityServer.Helper;
using CoreAPI.IdentityServer.Models;
using CoreAPI_IdentityServer;
using CoreAPI_IdentityServer.Redis;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Linq;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);



    
    Console.Title = "IdentityServerWithEfAndAspNetIdentity";

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .MinimumLevel.Override("System", LogEventLevel.Warning)
    .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
    .Enrich.FromLogContext()
    .WriteTo.File(@"Logs/identityserver4_log.txt")
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Literate)
    .CreateLogger();
var seed = args.Contains("/seed");
if (seed)
{
    args = args.Except(new[] { "/seed" }).ToArray();
}

builder.Services.AddSameSiteCookiePolicy();

var Connection = Appsettings.app(new string[] { "ConnectionStrings", "Connection" }).ObjToString();

var migrationsAssembly = typeof(Program).GetTypeInfo().Assembly.GetName().Name;//获取当前项目名
builder.Services.AddRedisCacheSetup();
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Connection));
builder.Services.Configure<IdentityOptions>(
 options =>
 { });
builder.Services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
{
    options.User = new UserOptions
    {
        RequireUniqueEmail = true, //要求Email唯一
        AllowedUserNameCharacters = null //允许的用户名字符
    };
    options.Password = new PasswordOptions
    {
        RequiredLength = 8, //要求密码最小长度，默认是 6 个字符
        RequireDigit = true, //要求有数字
        RequiredUniqueChars = 3, //要求至少要出现的字母数
        RequireLowercase = true, //要求小写字母
        RequireNonAlphanumeric = false, //要求特殊字符
        RequireUppercase = false //要求大写字母
    };
})
.AddEntityFrameworkStores<ApplicationDbContext>()
.AddDefaultTokenProviders();
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = new PathString("/oauth2/authorize");
});
//配置session的有效时间,单位秒
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromSeconds(30);
});
builder.Services.AddMvc();
builder.Services.Configure<IISOptions>(iis =>
{
    iis.AuthenticationDisplayName = "Windows";
    iis.AutomaticAuthentication = false;
});

var builders=  builder.Services.AddIdentityServer(
    options =>
    {
        options.Events.RaiseErrorEvents = true;
        options.Events.RaiseInformationEvents = true;
        options.Events.RaiseFailureEvents = true;
        options.Events.RaiseSuccessEvents = true;
                    // 查看发现文档
                    if (builder.Configuration["StartUp:IsOnline"].ObjToBool())
        {
            options.IssuerUri = builder.Configuration["StartUp:OnlinePath"].ObjToString();
        }
        options.UserInteraction = new IdentityServer4.Configuration.UserInteractionOptions
        {
            LoginUrl = "/oauth2/authorize",//登录地址  
                    };
    })
   // 自定义验证
   .AddExtensionGrantValidator<WeiXinOpenGrantValidator>()
    // 数据库模式
    .AddAspNetIdentity<ApplicationUser>()
    .AddConfigurationStore(options =>
    {
        options.ConfigureDbContext = b => b.UseSqlServer(Connection, sql => sql.MigrationsAssembly(migrationsAssembly));
    })
    // this adds the operational data from DB (codes, tokens, consents)
    .AddOperationalStore(options =>
    {
        options.ConfigureDbContext = b => b.UseSqlServer(Connection, sql => sql.MigrationsAssembly(migrationsAssembly));
        options.EnableTokenCleanup = true;
    });
builders.AddDeveloperSigningCredential();

if (builder.Environment.IsDevelopment())
{
    builders.AddDeveloperSigningCredential();
}
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Admin", policy => policy.Requirements.Add(new ClaimRequirement("rolename", "Admin")));
    options.AddPolicy("SuperAdmin", policy => policy.Requirements.Add(new ClaimRequirement("rolename", "SuperAdmin")));
});

builder.Services.AddSingleton<IAuthorizationHandler, ClaimsRequirementHandler>();

var app = builder.Build();
if (seed)
    SeedData.EnsureSeedData(app.Services);
app.Use(async (ctx, next) =>
{
    if (builder.Configuration["StartUp:IsOnline"].ObjToBool())
    {
        ctx.SetIdentityServerOrigin(builder.Configuration["StartUp:OnlinePath"].ObjToString());
    }
    await next();
});
app.UseCookiePolicy();
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseSession();
app.UseStaticFiles();
app.UseRouting();
app.UseIdentityServer();
app.UseAuthentication();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});

app.Run();
builder.Host.ConfigureWebHostDefaults(webBuilder =>
{
    webBuilder.ConfigureKestrel(serverOptions =>
    {
        serverOptions.AllowSynchronousIO = true;//启用同步 IO
    })
// .UseStartup<Program>()
.ConfigureLogging((hostingContext, builder) =>
{
    builder.ClearProviders();
    builder.SetMinimumLevel(LogLevel.Trace);
    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
    builder.AddConsole();
    builder.AddDebug();
}).ConfigureAppConfiguration((hostingContext, config) =>
{
    config.Sources.Clear();
    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
});
});
