﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace CoreAPI_IdentityServer
{
    public class Config
    {
        public Config()
        {

        }
        public static IEnumerable<IdentityResource> IdentityResources =>
          new List<IdentityResource>
          {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource("roles", "角色", new List<string> { JwtClaimTypes.Role }),
                new IdentityResource("rolename", "角色名", new List<string> { "rolename" }),
          };
        public static IEnumerable<ApiResource> ApiResource()
        {
            return new List<ApiResource>
            {
                new ApiResource("coreapi", "CoreAPI"){
                UserClaims = { JwtClaimTypes.Name, JwtClaimTypes.Role, "rolename" },
                Scopes = { "coreapi", "CoreAPI" },
                ApiSecrets = new List<Secret>()
                    {
                        new Secret("api_secret".Sha256())
                    },
               }
            };
        }
        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
            new ApiScope("coreapi")
            };
        public static IEnumerable<Client> Clients =>
            new[]{ new Client
            {
                    ClientId = "blogadminjs",
                    ClientName = "Blog.Admin JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =
                    {
                        "http://vueadmin.neters.club/callback",
                        "http://apk.neters.club/oauth2-redirect.html",

                        "http://localhost:2364/callback",
                        "http://localhost:8081/oauth2-redirect.html",
                    },
                    PostLogoutRedirectUris = { "http://vueadmin.neters.club","http://localhost:2364" },
                    AllowedCorsOrigins =     { "http://vueadmin.neters.club","http://localhost:2364"  },

                    AccessTokenLifetime=3600,
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "roles",
                        "coreapi"
                    }
                 }
            };
    }
}
