﻿using CoreAPI.Common.Appseting;
using CoreAPI.Common.HttpContextUser;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace CoreAPI.Common.Helper
{
    public class AspNetUser : IUser
    {
        private readonly IHttpContextAccessor _accessor;
        public AspNetUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }


        public string IP => GetIpAddress();


        public string LocalIP => GetLocalIPAddress();

        public string HostName => GetHostName();

        public string Name => throw new System.NotImplementedException();

        public int ID => throw new System.NotImplementedException();



        /// <summary>
        /// 获取本机访问的IP地址
        /// </summary>
        /// <returns></returns>
        private static string GetLocalIPAddress()
        {
            try
            {
                string ip = "";
                string hostName = Dns.GetHostName();
                System.Net.IPAddress[] addressList = Dns.GetHostAddresses(hostName);
                for (int i = 0; i < addressList.Length; i++)
                {
                    if (addressList[i].AddressFamily.ToString() == "InterNetwork")
                    {
                        ip = addressList[i].ToString();
                    }
                }
                return ip;
            }
            catch
            {
                return "";
            }
        }

        /// <summary> 
        /// 获取主机名 
        /// </summary> 
        /// <returns></returns> 
        private string GetHostName()
        {
            try
            {
                return System.Net.Dns.GetHostName();
            }
            catch
            {
                return "";
            }
        }


        /// <summary>
        /// 远程访问的IP
        /// </summary>
        /// <returns></returns>
        private string GetIpAddress()
        {
            var ip = _accessor.HttpContext.Request.Headers["X-Forwarded-For"].ObjToString();
            if (string.IsNullOrEmpty(ip))
            {
                ip = _accessor.HttpContext.Connection.RemoteIpAddress.ObjToString();
            }
            return ip;
        }

  


        public string GetToken()
        {
            return _accessor.HttpContext.Request.Headers["Authorization"].ObjToString().Replace("Bearer ", "");
        }

        public bool IsAuthenticated()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Claim> GetClaimsIdentity()
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetClaimValueByType(string ClaimType)
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetUserInfoFromToken(string ClaimType)
        {
            throw new System.NotImplementedException();
        }
    }

}
