﻿using System;

namespace CoreAPI.DTO
{
    public class RedisKeyDto
    {
        public RedisKeyDto(){ }
        public RedisKeyDto(RedisKeyDto redis)
        {
            RedisKeyID = redis.RedisKeyID;
            RedisName = redis.RedisName;
            btitle = redis.btitle;
            SateTime = redis.SateTime;
            bUpdateTime = redis.bUpdateTime;
            bCreateTime =DateTime.Now;
            bRemark = redis.bRemark;
            IsDeleted = false;
        }
        public int RedisKeyID { get; set; }
        /// <summary>
        /// rediskey前缀
        /// </summary>
        public string RedisName { get; set; }
        /// <summary>
        /// 标题redis
        /// </summary>
        public string btitle { get; set; }
        /// <summary>
        /// 类别
        /// </summary>
        public string RedisType { get; set; }
        /// <summary>
        /// 缓存时间
        /// </summary>
        public int SateTime { get; set; }
        /// <summary> 
        /// 修改时间
        /// </summary>
        public DateTime bUpdateTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime bCreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 备注
        /// </summary>
        public string bRemark { get; set; }
        /// <summary>
        /// 逻辑删除
        /// </summary>
        public bool? IsDeleted { get; set; }
    }
}
