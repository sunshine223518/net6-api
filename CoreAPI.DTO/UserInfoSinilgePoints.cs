﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAPI.DTO
{
    public class UserInfoSinilgePoints
    {
        public string UserName { get;  set; }
        public DateTime LastTime { get;  set; }
        public string OnlyCode { get;  set; }
        public string Ip { get;  set; }

    }
}
