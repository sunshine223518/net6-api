﻿using CoreAPI.DTO;
using AutoMapper;
using CoreAPI.Model.Models;

namespace CoreAPI.Extensions.AutoMapper
{
    /// <summary>
    ///  配置构造函数，用来创建关系映射
    /// </summary>
    public class CustomProfile : Profile
    {
        public CustomProfile()
        {
            CreateMap<sysUserInfo, sysUserInfoDTO>();
            CreateMap<sysUserInfoDTO, sysUserInfo>();
            CreateMap<RedisKeyDto, RedisKey>();
            CreateMap<RedisKey, RedisKeyDto>();
        }
    }
}
