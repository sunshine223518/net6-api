﻿using CoreAPI.EventBus.EventBus;

namespace CoreAPI.Extensions.EventHanding
{
    public class QuartzJobReCoveryIntegrationEvent : IntegrationEvent
    {
        public string JobID{ get; private set; }
        /// <summary>
        /// 此处的jobid必须和 JobID 保持相同不管大小写 JobiD同样可以因为是private set
        /// </summary>
        /// <param name="jobid"></param>
        public QuartzJobReCoveryIntegrationEvent(string jobid)=>JobID =jobid;
        
    }
}
