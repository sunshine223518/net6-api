﻿using CoreAPI.Common.Helper;
using CoreAPI.EventBus.EventBus;
using CoreAPI.IService;
using CoreAPI.Tasks;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CoreAPI.Extensions.EventHanding
{
    public class QuartzJobStopIntegrationEventHandler : IIntegrationEventHandler<QuartzJobReCoveryIntegrationEvent>
    {
        private readonly ITasksQzServices _tasksQzServices;
        private readonly ISchedulerCenter _schedulerCenter;
        private readonly ILogger<QuartzJobStopIntegrationEventHandler> _logger;

        public QuartzJobStopIntegrationEventHandler(
            ITasksQzServices tasksQzServices,
            ISchedulerCenter schedulerCenter,
            ILogger<QuartzJobStopIntegrationEventHandler> logger
            )
        {
            this._tasksQzServices = tasksQzServices;
            this._schedulerCenter = schedulerCenter;
            this._logger = logger;
        }

        public async Task Handle(QuartzJobReCoveryIntegrationEvent @event)
        {
            _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, "CoreAPI", @event);
            ConsoleHelper.WriteSuccessLine($"----- Handling integration event: {@event.Id} at CoreAPI - ({@event})");

            var model = await _tasksQzServices.QueryById(@event.JobID.ToString());
            var success = false;
            if (model != null)
            {
                try
                {
                    var flag = await _tasksQzServices.DeleteById(@event.JobID.ToString());
                    if (flag)
                        success = true;
                }
                catch (Exception)
                {
                    throw;
                }
                Console.WriteLine("任务！" + success);

            }
            else
                Console.WriteLine("任务不存在！" + success);
        }
    }
}
