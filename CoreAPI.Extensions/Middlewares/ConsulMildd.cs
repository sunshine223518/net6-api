﻿using Consul;
using CoreAPI.Common.Appseting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;

namespace CoreAPI.Extensions.Middlewares
{
    /// <summary>
    /// Consul 注册服务
    /// </summary>
    public static class ConsulMildd
    {
        public static IApplicationBuilder UseConsulMildd(this IApplicationBuilder app, IConfiguration configuration, IHostApplicationLifetime lifetime)
        {
            var tt = configuration["Middleware:Consul:Enabled"].ObjToBool();
            if (configuration["Middleware:Consul:Enabled"].ObjToBool())
            {
                var consulClient = new ConsulClient(c =>
               {
                   //consul地址
                   c.Address = new Uri(configuration["ConsulSetting:consulAddress"]);
                   c.Datacenter = "dc1";
               });
    
                string currentIp = configuration["ConsulSetting:currentIP"];
                string currentPort = configuration["ConsulSetting:currentPort"];

                string serviceId = $"service:{currentIp}:{currentPort}";//服务ID，一个服务是唯一的
                consulClient.Agent.ServiceRegister(new AgentServiceRegistration()
                {
                    ID = serviceId, //唯一的
                    Name = configuration["ConsulSetting:serviceName"], //组名称-Group
                    Address = currentIp, //ip地址
                    Port = int.Parse(currentPort), //端口
                    Tags = new string[] { "api站点" },
                    Check = new AgentServiceCheck()
                    {
                        Interval = TimeSpan.FromSeconds(10),//多久检查一次心跳
                        HTTP = $"http://{currentIp}:{currentPort}/api/V2/HealthCheck/Index",
                        Timeout = TimeSpan.FromSeconds(5),//超时时间
                        DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5) //服务停止多久后注销服务
                    }
                }).Wait();
                //var registration = new AgentServiceRegistration()
                //{
                //    //  ID = "service " + configuration["ConsulSetting:ServiceIP"] + ":" + configuration["ConsulSetting:ServicePort"],//Ray--唯一的
                //    ////  ID = Guid.NewGuid().ToString(),//服务实例唯一标识
                //    //  Name = configuration["ConsulSetting:ServiceName"],//服务名
                //    //  Tags=new string[] { configuration["ConsulSetting:weight"] },//权重
                //    //  Address = configuration["ConsulSetting:ServiceIP"], //服务IP
                //    //  Port = int.Parse(configuration["ConsulSetting:ServicePort"]),//服务端口
                //    //  Check = new AgentServiceCheck()
                //    //  {


                //    //      DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5),//服务启动多久后注册
                //    //      Interval = TimeSpan.FromSeconds(10),//健康检查时间间隔
                //    //      HTTP = $"http://{configuration["ConsulSetting:ServiceIP"]}:{configuration["ConsulSetting:ServicePort"]}/Api/Health/Index",
                //    //      //HTTP = $"http://{configuration["ConsulSetting:ServiceIP"]}:{configuration["ConsulSetting:ServicePort"]}{configuration["ConsulSetting:ServiceHealthCheck"]}",//健康检查地址
                //    //      Timeout = TimeSpan.FromSeconds(5)//超时时间
                //    //  }
                //};

                ////服务注册
                //consulClient.Agent.ServiceRegister(registration).Wait();

                //应用程序终止时，取消注册
                lifetime.ApplicationStopping.Register(() =>
                {
                    consulClient.Agent.ServiceDeregister(serviceId).Wait();
                });

            }
            return app;
        }
    }
}
