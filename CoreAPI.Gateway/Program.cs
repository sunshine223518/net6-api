using CoreAPI.Extensions;
using CoreAPI.ServiceExtensions;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Ocelot.Provider.Polly;


/**
    *┌──────────────────────────────────────────────────────────────┐
    *│　描    述：模拟一个网关项目         s         
    *│　作    者：sunshine                                             
    *└──────────────────────────────────────────────────────────────┘
    */

var builder = WebApplication.CreateBuilder ();

//Add services to the container.
//这种写法也可以
//builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
//{
//    config.AddJsonFile("configuration.json", optional: true, reloadOnChange: true);//网关配置
//});
builder.Configuration.AddJsonFile("configuration.json", optional: true, reloadOnChange: true);
builder.Services.AddAuthentication_JWTSetup();
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCorsSetup();
builder.Services.AddOcelot().AddConsul().AddPolly();
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.UseRouting();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.MapControllers();
app.UseAuthentication();
app.UseAuthorization();
app.UseOcelot();//网关
app.Run();
