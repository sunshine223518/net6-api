using CoreAPI.IService.BASE;
using CoreAPI.Model.Identity;
using CoreAPI.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreAPI.IServices
{
    /// <summary>
    /// RoleModulePermissionServices
    /// </summary>	
    public interface IRoleModulePermissionServicesNew :IBaseServices<RoleModulePermissions>
	{

        Task<List<RoleModulePermissions>> GetRoleModule(int roleID);
        Task<List<TestMuchTableResult>> QueryMuchTable();
        Task<List<RoleModulePermissions>> RoleModuleMaps();
        Task<List<RoleModulePermissions>> GetRMPMaps();
        /// <summary>
        /// 批量更新菜单与接口的关系
        /// </summary>
        /// <param name="permissionId">菜单主键</param>
        /// <param name="moduleId">接口主键</param>
        /// <returns></returns>
        Task UpdateModuleId(int permissionId, int moduleId);
    }
}
