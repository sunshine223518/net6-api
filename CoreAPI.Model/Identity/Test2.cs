﻿using SqlSugar;

namespace CoreAPI.Model.Identity
{
    [SugarTable("Test2", "Identity")]
    public class Test2
    {
        [SugarColumn(IsNullable = false, IsPrimaryKey = true, IsIdentity = true)]
        public int ID {  get; set; }
        public string Values2 {  get; set; }
    }
}
