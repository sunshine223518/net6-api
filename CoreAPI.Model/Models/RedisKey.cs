﻿using SqlSugar;
using System;

namespace CoreAPI.Model.Models
{
    /// <summary>
    /// redis缓存key
    /// </summary>
    [SugarTable("RedisKey", "CoreAPI")]
   
    public class RedisKey
    {
        /// <summary>
        /// 主键
        /// </summary>
        /// 这里之所以没用RootEntity，是想保持和之前的数据库一致，主键是RedisKeyID，不是Id
        [SugarColumn(IsNullable = false, IsPrimaryKey = true, IsIdentity = true)]
        public int RedisKeyID { get; set;}
        /// <summary>
        /// rediskey前缀
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 600, IsNullable = true)]
        public string RedisName { get; set; }
        /// <summary>
        /// 标题redis
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 256, IsNullable = true)]
        public string btitle { get; set; }
        /// <summary>
        /// 类别
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 2000, IsNullable = true)]
        public string RedisType { get; set; }
        /// <summary>
        /// 缓存时间
        /// </summary>
        public int SateTime { get; set; }
        /// <summary> 
        /// 修改时间
        /// </summary>
        public DateTime bUpdateTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime bCreateTime { get; set; } 
        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 2000, IsNullable = true)]
        public string bRemark { get; set; }
        /// <summary>
        /// 逻辑删除
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public bool? IsDeleted { get; set; }
    }
}
