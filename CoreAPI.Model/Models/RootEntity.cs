﻿using SqlSugar;

namespace CoreAPI.Model
{
    [SugarTable("RootEntity", "CoreAPI")]
    public class RootEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        [SugarColumn(IsNullable = false, IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }

      
    }
}