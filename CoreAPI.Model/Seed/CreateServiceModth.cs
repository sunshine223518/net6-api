﻿using CoreAPI.Common.Appseting;
using CoreAPI.Common.Helper;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;

namespace CoreAPI.Model.Seed
{
    public static class CreateServiceModth
    {

        public static string SolutionName = Appsettings.app(new string[] { "Startup", "ApiName" });
        public static Dictionary<string, string> ProjectIds = new Dictionary<string, string>();
        public static string GetCurrentProjectPath
        {
            get
            {
                   return Environment.CurrentDirectory.Replace(@"\bin\Debug", "").Replace("\\netcoreapp3.1", "");
            }
        }
        public static string GetSlnPath
        {
            get
            {
                var path = Directory.GetParent(GetCurrentProjectPath).FullName;
                return path;
            }
        }


        public static (bool, string) CreateLogic(string templatePath, string savePath, string tables, string usingstr, string classNamespace, string keyname, string suffix)
        {

            try
            {
                string template = File.ReadAllText(templatePath); //从文件中读出模板内容
                string templateKey = keyname; //取个名字

                BLLParameter model = new BLLParameter()
                {
                    Name = tables,
                    ClassNamespace = classNamespace,
                    usingstr = usingstr
                };
                var result = Engine.Razor.RunCompile(template, templateKey, model.GetType(), model);
                if (classNamespace == "CoreAPI.IService" || classNamespace == "CoreAPI.IRepository")
                {
                    tables = "I" + tables;
                }
                var cp = savePath + "\\" + tables + $"{suffix}.cs";
                if (FileHelper.IsExistFile(cp) == false)
                    FileHelper.CreateFile(cp, result, System.Text.Encoding.UTF8);
                else
                    return (false, $"{tables}{suffix}已经存在，生成失败\n");

                return (true, $"{tables}{suffix}生成成功\n");
            }
            catch (Exception ex)
            {

                return (false, $"{suffix}生成失败/n");
            }
        }

        /// <summary>
        /// 生成底层逻辑
        /// </summary>
        /// <param name="tables">表名称</param>
        /// <param name="usingstr">引用字符串，读取配置文件appsetting</param>
        /// <param name="projectname">项目名称</param>
        /// <param name="tempname">模板名称</param>
        /// <param name="suffix">生成文件后缀名称</param>
        /// <param name="otherdbfilepath"></param>
        /// <param name="savePath"></param>
        /// <returns></returns>
        public static void GenerationLogic(string savePath, string tables, string usingstr, string projectname, string tempname, string suffix, string otherdbfilepath = "")
        {
            var bllProjectName2 = SolutionName + $".{projectname}";//具体项目

           // var savePath2 = savePath;//保存目录
            var savePath2 = Directory.GetParent(GetCurrentProjectPath).FullName + "\\" 
                + bllProjectName2 + (otherdbfilepath == "" ? "" : "\\" + otherdbfilepath)+"\\Test";//保存目录

            var templatePath = GetCurrentProjectPath+ ".Extensions\\CreateService" + $"\\Template\\{tempname}.txt";//bll模版地址  这里改为 frame里面的模板 地址

            var res = CreateLogic(templatePath, savePath2, tables, usingstr, bllProjectName2, tempname.ToLower(), suffix);
            if (res.Item1)
                ConsoleHelper.WriteSuccessLine(res.Item2);
            else
                ConsoleHelper.WriteErrorLine(res.Item2);
        }

    }
    public class BLLParameter
    {
        public string Name { get; set; }
        public string ClassNamespace { get; set; }

        public string usingstr { get; set; }
    }
}
