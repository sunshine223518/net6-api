﻿using SqlSugar;
using System;
using System.Linq;
using System.Reflection;

namespace CoreAPI.Model.Seed
{
    public class DbFrist
    {
        private static string bllusing = $"using {CreateServiceModth.SolutionName}.Model;\n using {CreateServiceModth.SolutionName}.IService;\n using {CreateServiceModth.SolutionName}.IRepository.Base;\n";
        private static string ibllusing = $"using  {CreateServiceModth.SolutionName}.Model;\n using  {CreateServiceModth.SolutionName}.IService;";
        /// <summary>
        /// 根据主库生成model 默认第一个地址
        /// </summary>
        /// <param name="myContext"></param>
        /// <param name="programpath"></param>
        public static void CreateSingleDataBaseByDBAsync(MyContext myContext, string programpath)
        {
            try
            {
                //获取项目绝对路径
                string[] arr = programpath.Split("\\");
                string path = "";
                for(int i = 0; i < arr.Length-1; i++)
                {
                    path += arr[i];
                    path += "\\";
                }
                var filePath = path + "CoreAPI.Model\\" + "Test";
                //获取实体名
                var modelTypes=from t in Assembly.GetExecutingAssembly().GetTypes()
                              where t.IsClass && t.Namespace == "CoreAPI.Model.Test" && t.GetCustomAttributes(typeof(SugarTable), true).FirstOrDefault((x => x.GetType()
                              ==typeof(SugarTable))) is SugarTable sugarTable &&string.IsNullOrEmpty(sugarTable.TableDescription)
                                select t.Name;
                //获取数据表
                var tablelist = myContext.Db.DbMaintenance.GetTableInfoList(false);

                tablelist.ForEach(t =>
                {

                    //生成数据库表
                    if (!modelTypes.ToList().Contains(t.Name))
                    {
                        //生成实体类
                        myContext.Db.DbFirst.IsCreateAttribute().Where(t.Name).CreateClassFile(filePath, "CoreAPI.Model.Test");
                        Console.WriteLine(t.Name+"实体生成成功！");
                        //生成对应层类文件
                        CreateServiceModth.GenerationLogic("CoreAPI.Service", t.Name, bllusing, "Service", "Service", "Service");
                        CreateServiceModth.GenerationLogic("CoreAPI.IService", t.Name, ibllusing, "IService", "IService", "IService");
                    }
                });
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
