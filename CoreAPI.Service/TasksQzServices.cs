﻿using CoreAPI.IService;
using CoreAPI.Model.BASE;
using CoreAPI.Model.Models;
using CoreAPI.Repository.BASE;
using CoreAPI.Repository.UnitOfWork;
using CoreAPI.Service.BASE;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreAPI.Service
{
    /// <summary>
    ///任务调度
    /// </summary>
    public partial class TasksQzServices : BaseServices<TasksQz>, ITasksQzServices
    {
        IBaseRepository<TasksQz> _dal;
        private readonly IUnitOfWork unitOfWork;

        public TasksQzServices(IBaseRepository<TasksQz> dal,IUnitOfWork unitOfWork)
        {
            this._dal = dal;
            this.unitOfWork = unitOfWork;
            base.BaseDal = dal;
        }
        public async Task<PageModels<TasksQz>> GetPage(int pageIndex, int PageSize)
        {
            PageModels<TasksQz> models = new();
            models = await QueryPage(a => a.IsDeleted != true, pageIndex, PageSize, "Id desc ");
            return models;
        }
        public async  Task<int> TasksQzAdd(TasksQz p)
        {
            return await Add(p);
        }
        public async Task<bool> TasksQzUpdate(TasksQz p)
        {
            return await Update(p);
        }

        public async Task<int> TasksQzAdda()
        {
            List<Guestbook> list = new();
           
            Console.WriteLine("种子数据构建开始时间：" + DateTime.Now);
            for (int i = 0; i < 100000; i++)
            {
                Guestbook list1 = new();
                list1. isshow = false;
                list1. blogId = i;
                list1.createdate = DateTime.Now;
                list.Add(list1);
            }
            Console.WriteLine("种子数据构建结束时间：" + DateTime.Now + "；一共准备插入种子" + list.Count+"条数据");
            var db = unitOfWork.GetDbClient();
            var time = DateTime.Now;
            Console.WriteLine("种子数据插入开始时间：" + time);
            _= await db.Insertable(list).UseSqlServer().ExecuteBulkCopyAsync(); 
             var times = DateTime.Now - time;
            Console.WriteLine("插入完成时间：" + DateTime.Now + ";一共耗时："+ times + "；一共准备插入种子" + list.Count + "条数据");
            
            return 1;
        }
    }
}
