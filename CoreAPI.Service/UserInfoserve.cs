﻿using AutoMapper;
using CoreAPI.Common;
using CoreAPI.Common.Appseting;
using CoreAPI.DTO;
using CoreAPI.IService;
using CoreAPI.Model.Identity;
using CoreAPI.Model.Models;
using CoreAPI.Repository.BASE;
using CoreAPI.Repository.UnitOfWork;
using CoreAPI.Service.BASE;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Service
{
    public class UserInfoserve : BaseServices<sysUserInfo>, IUserInfoIserve
    {
     
        private readonly IBaseRepository<sysUserInfo> _dal;
        readonly IBaseRepository<Role> _roleRepository;
        private readonly IBaseRepository<UserRole> _userRoleRepository;
        private readonly IBaseRepository<RedisKey> _RedisKey;
        private readonly IBaseRepository<Test2> test;
        private readonly IUnitOfWork db;
        private readonly IMapper _mapper;

        public UserInfoserve(
            IBaseRepository<sysUserInfo> dal,
            IBaseRepository<Role> roleRepository,
            IBaseRepository<UserRole> userRoleRepository,
            IBaseRepository<RedisKey> RedisKey,
            IBaseRepository<Test2> Test,
            IUnitOfWork Db,
            IMapper mapper)
        {
            this.BaseDal = dal;
            this._dal = dal;
            this._roleRepository = roleRepository;
            this._userRoleRepository = userRoleRepository;
            this._RedisKey = RedisKey;
            test = Test;
            db = Db;
            this._mapper = mapper;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="UserLog"></param>
        /// <param name="UserPwd"></param>
        /// <returns></returns>
        public async Task<sysUserInfo> GetLists(string UserLog, string UserPwd)
        {
           
            var cc = await Query();
            var t = cc[0];
            t.sex = 26;

            await Update(t);
            sysUserInfo model = null;
            var list = await Query(a => a.uLoginName == UserLog && a.uLoginPWD == UserPwd && a.tdIsDelete == false);
            //var list1 = await Query1(a => a.tdIsDelete == false,b=>b.uID, c=>c.uID,true);//bug
            if (list.Count > 0)
            {
                model = list.FirstOrDefault();
                var time = DateTime.Now.AddHours(20) - DateTime.Now;
                RedisKeyDto dto = new()
                {
                    RedisName = model.uLoginName,
                    RedisType = "登录",
                    btitle = "缓存Token",
                    bRemark = "退出关闭浏览器清除token",
                    SateTime = 2,
                    bUpdateTime = DateTime.Now,
                    IsDeleted = false,
                };
                //后期写入redis缓存
                var mapper = _mapper.Map<RedisKey>(dto);
                var rediskey = await _RedisKey.Add(mapper);
                //更具条件修改
               // var uselist = await base.Update(model, "uID="+model.uID);
                //更具主键修改
                //var uselist1 = await base.Update(model);
            }
            return model;
        }
        /// <summary>
        /// 测试缓存
        /// </summary>
        /// <param name="UserLog"></param>
        /// <param name="UserPwd"></param>
        /// <returns></returns>
        [Caching(AbsoluteExpiration = 30)]//缓存30分钟
        public async Task<List<sysUserInfo>> GetList(string uLoginName)
        {
            return await Query(u=>u.uLoginName==uLoginName);
        }
        /// <summary>
        /// 测试添加
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public async Task<sysUserInfo> Adds(sysUserInfoDTO p)
        {
            sysUserInfo models = _mapper.Map<sysUserInfo>(p);
         //   var  ids=  await test.Add(new Test2 { Values2 = "添加成功" });
            var list = await Add(models);
            sysUserInfo model = new();
            if (list > 0)
            {
                model = await QueryById(list);
            }
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="loginPwd"></param>
        /// <returns></returns>

        public async Task<string> GetUserRoleNameStr(string loginName, string loginPwd)
        {
            string roleName = "";
            var user = (await Query(a => a.uLoginName == loginName && a.uLoginPWD == loginPwd)).FirstOrDefault();
            var roleList = await _roleRepository.Query(a => a.IsDeleted == false);
            if (user != null)
            {
                var userRoles = await _userRoleRepository.Query(u => u.UserId == user.uID);
                if (userRoles.Count > 0)
                {
                    var arr = userRoles.Select(ur => ur.RoleId.ObjToString()).ToList();
                    var roles = roleList.Where(d => arr.Contains(d.Id.ObjToString()));
                    roleName = string.Join(',', roles.Select(r => r.Name).ToArray());
                }
            }
            return roleName;
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public async Task<bool> UpdatePwd(sysUserInfoDTO p)
        {
            bool relust = false;
            sysUserInfo model = _mapper.Map<sysUserInfo>(p);
            //验证原来密码
            var list = (await Query(a => a.uLoginPWD ==model.uLoginPWD&&a.uLoginName== model.uLoginName && a.tdIsDelete == false)).FirstOrDefault();
            if (list != null)
            {
                var pwd = await base.Update(p);
                relust = true;
            }
            return relust;
        }
    }
}
