﻿using CoreAPI.IService;
using CoreAPI.Model.Identity;
using CoreAPI.Repository.BASE;
using CoreAPI.Service.BASE;

namespace CoreAPI.Service
{
    public class Test : BaseServices<AspNetRoles>, Itest
    {
        private readonly IBaseRepository<AspNetRoles> dal;

        public Test(IBaseRepository<AspNetRoles> dal)
        {
            this.dal = dal;
            this.BaseDal = dal;
        }
    }
}
