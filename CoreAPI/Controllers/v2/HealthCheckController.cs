﻿using CoreAPI.Filter;
using Microsoft.AspNetCore.Mvc;
using static CoreAPI.ServiceExtensions.CustomApiVersion;

namespace CoreAPIController.Controllers.v2
{
    /// <summary>
    /// 健康检查
    /// </summary>
    [CustomRoute(ApiVersions.V2)]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
        /// <summary>
        /// 健康检查接口
        /// </summary>
        /// <returns></returns>
        [HttpGet("Index")]
        public string Index()
        {
            return "ok";
        }
    }
}