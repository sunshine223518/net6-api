﻿
using AutoMapper;
using CoreAPI.Common.Appseting;
using CoreAPI.Common.Helper;
using CoreAPI.Core;
using CoreAPI.DTO;
using CoreAPI.Extensions.Authorizations.Policys;
using CoreAPI.Extensions.Redis;
using CoreAPI.Filter;
using CoreAPI.IRepository;
using CoreAPI.IService;
using CoreAPI.IServices;
using CoreAPI.Model.BASE;
using CorePAI.Authorizations.OverWrite;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static CoreAPI.ServiceExtensions.CustomApiVersion;

namespace CoreAPIController.Controllers.v2
{
    /// <summary>
    /// 用户登录
    /// </summary>
    [CustomRoute(ApiVersions.V2)]
    //[Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserInfoIserve _userInfo;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IMapper mapper;
        private readonly IRedisBasketRepository _redis;
        private readonly IRoleModulePermissionServices _roleModulePermissionServices;
        private readonly PermissionRequirement _requirement;
        private static readonly object LockObject = new object();
        /************************************************/
        // 如果需要使用Http协议带名称的，比如这种 [HttpGet("apbs")]
        // 目前只能把[CustomRoute(ApiVersions.v2)] 提到 controller 的上边，做controller的特性
        // 并且去掉//[Route("api/[controller]")]路由特性，否则会有两个接口
        /************************************************/

        public LoginController(IUserInfoIserve userInfo,
            IHttpContextAccessor httpContext,
            IMapper mapper,
            PermissionRequirement requirement, IRedisBasketRepository redis,
            IRoleModulePermissionServices roleModulePermissionServices)
        {
            _userInfo = userInfo;
            this._httpContext = httpContext;
            this.mapper = mapper;
            _redis = redis;
            _roleModulePermissionServices = roleModulePermissionServices;
            _requirement = requirement;
        }
        /// <summary>
        /// 基于策略token方案1 1000s过期
        /// 实现两种方案（目前采用方案1)
        /// 1 已经登录的用户不能在进行登录除非退出
        /// 2.当前登录的用户直接踢下线
        /// </summary>
        /// <param name="UserLog"></param>
        /// <param name="UserPwd"></param>
        /// <returns></returns>
        [HttpPut("GetToken")]
        public async Task<MessageModel<TokenInfoViewModel>> GetToken(string UserLog, string UserPwd)
        {
            string jwtStr = string.Empty;
            var date = DateTime.Now.AddSeconds(1000) - DateTime.Now;
            if (string.IsNullOrEmpty(UserLog) || string.IsNullOrEmpty(UserPwd))
                return new MessageModel<TokenInfoViewModel>()
                {
                    success = false,
                    msg = "用户名或密码不能为空",
                };
            //目前最简单模式将所有key都进行缓存，最常见的则是采用布隆过滤器，将所有可能存在的数据哈希到
            // 一个足够大的bitmap中，一个一定不存在的数据会被 这个bitmap拦截掉，从而避免了对底层存储系统的查询压力。
            string c = $"{UserLog}s";
            var GetExist = await _redis.Exist(c);
            switch (GetExist)
            {
                case false:
                    {
                        bool GetExists = await _redis.Exist(UserLog);
                        if (GetExists == false)
                        {
                            var user = await _userInfo.GetLists(UserLog, MD5Helper.MD5Encrypt32(UserPwd));
                            if (user != null)
                            {
                                var userRoles = await _userInfo.GetUserRoleNameStr(UserLog, MD5Helper.MD5Encrypt32(UserPwd));
                                string ipaddress = _httpContext.HttpContext.Connection.RemoteIpAddress.ToString();
                                //如果是基于用户的授权策略，这里要添加用户;如果是基于角色的授权策略，这里要添加角色
                                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, UserLog),
                    new Claim(ClaimTypes.Uri, ipaddress),
                    new Claim(JwtRegisteredClaimNames.Jti, user.uID.ToString()),
                    new Claim(ClaimTypes.Expiration, DateTime.Now.AddSeconds(_requirement.Expiration.TotalSeconds).ToString()) };
                                claims.AddRange(userRoles.Split(',').Select(s => new Claim(ClaimTypes.Role, s)));
                                // jwt
                                if (!Permissions.IsUseIds4)
                                {
                                    var data = await _roleModulePermissionServices.RoleModuleMaps();
                                    var list = (from item in data
                                                where item.IsDeleted == false
                                                orderby item.Id
                                                select new PermissionItem
                                                {
                                                    Url = item.Module?.LinkUrl,
                                                    Role = item.Role?.Name.ObjToString(),
                                                }).ToList();
                                    _requirement.Permissions = list;
                                }
                                var token = JwtToken.BuildJwtToken(claims.ToArray(), _requirement);
                                //var onlycode=  MachineCodeHelper.GetOnly();   //这里是获取设备码
                                //await  _redis.Set(onlycode, onlycode, date);
                                await _redis.Set(UserLog, token, date);
                                return new MessageModel<TokenInfoViewModel>()
                                {
                                    success = true,
                                    msg = "获取成功",
                                    response = token
                                };
                            }
                            else
                            {
                                await _redis.Remove(c[0..^1]);//
                                await _redis.Set(c, c, date);
                                return new MessageModel<TokenInfoViewModel>()
                                {
                                    success = false,
                                    msg = "当前账号不存在！",
                                };
                            }
                        }
                        else
                        {
                            return new MessageModel<TokenInfoViewModel>()
                            {
                                success = false,
                                msg = "当前账号已登录，请退出！",
                            };
                        }
                    }

                default:
                    return new MessageModel<TokenInfoViewModel>()
                    {
                        success = false,
                        msg = "当前账号不存在！",
                    };
            }
        }
        /// <summary>
        /// 方法一无策略获取token
        /// </summary>
        /// <param name="UserLog"></param>
        /// <param name="UserPwd"></param>
        /// <returns></returns>
        [HttpPost("GetTokens")]
        public async Task<MessageModel<string>> GetTokens(string UserLog, string UserPwd)
        {
            bool suc = false;
            string jwtStr;
            //该方法可能造成redis缓存穿透（解决方法请看GetToken）
            var exist = await _redis.Exist(UserLog);
            switch (exist)
            {
                case false:
                    {
                        var user = await _userInfo.GetLists(UserLog, MD5Helper.MD5Encrypt32(UserPwd));
                        var date = DateTime.Now.AddDays(1) - DateTime.Now;
                        if (user != null)
                        {
                            TokenModelJwt tokenModel = new() { Uid = 1, Role = UserLog };
                            jwtStr = JwtHelper.IssueJwt(tokenModel);
                            suc = true;
                            MessageModel<string> s = new();
                            s.msg = user.uLoginName;
                            s.response = jwtStr;
                            UserInfoSinilgePoints models = new UserInfoSinilgePoints();
                            await (_ = _redis.Set(UserLog, models, date)).ConfigureAwait(false);
                        }
                        else
                        {
                            jwtStr = "请输入正确的用户名或者密码！";
                        }
                        break;
                    }
                default:
                    jwtStr = "当前用户已登录！请先退出登录";
                    break;
            }
            return new MessageModel<string>()
            {
                success = suc,
                msg = suc ? "获取成功" : "获取失败",
                response = jwtStr
            };
        }
        /// <summary>
        /// 清除用户登录信息
        /// </summary>
        /// <param name="UserLog"></param>
        /// <returns></returns>
        [Authorize(Permissions.Name)]
        [HttpDelete("Delete")]
        public async Task<MessageModel<string>> Delete(string UserLog)
        {
            MessageModel<string> data = new();
            var exist = await _redis.Exist(UserLog);
            if (exist == true)
            {
                await (_ = _redis.Remove(UserLog));
                TokenModelJwt tokenModel = new() { Uid = 1, Role = UserLog };
                _ = JwtHelper.IssueJwt(tokenModel);
                data.success = true;
                data.msg = "退出成功";
            }
            return data;
        }
        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [Authorize(Permissions.Name)]//基于策略授权
        [HttpPost("Add")]
        public async Task<MessageModel<string>> Add(sysUserInfoDTO p)
        {
            var data = new MessageModel<string>();
            sysUserInfoDTO dTO = new(p);
            var flag = await _userInfo.GetList(p.uLoginName);
            switch (flag.Count)
            {
                case 0:
                    {
                        var list = await _userInfo.Adds(dTO);
                        data.success = list.uLoginName != null;
                        if (data.success)
                        {
                            data.success = true;
                            data.msg = "添加成功";
                        }
                        break;
                    }
                default:
                    data.success = true;
                    data.msg = "当前登录名已存在！";
                    break;
            }
            return data;
        }
        /// <summary>
        /// 测试 MD5 加密字符串
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet("Md5Password")]
        public string Md5Password(string password)
        {
            var flag = _redis.GetValue("admin");
            if (flag != null)
                Console.WriteLine("redis中获取");
            else
            {
                if (flag == null)
                {
                    lock (LockObject)
                    {
                        var exist = _redis.GetValue("admin");
                        if (exist==null)
                            Console.WriteLine("DB中获取");
                        else
                            Console.WriteLine("redis中获取");
                    }
                }
                else
                    Console.WriteLine("redis中获取");
            }




            return MD5Helper.MD5Encrypt32(password);
        }

    }
}
