﻿using CoreAPI.Common.Appseting;
using CoreAPI.Core;
using CoreAPI.Filter;
using CoreAPI.IService;
using CoreAPI.Model.BASE;
using CoreAPI.Model.Models;
using CoreAPI.Repository.UnitOfWork;
using CoreAPI.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using static CoreAPI.ServiceExtensions.CustomApiVersion;

namespace CoreAPI.Controllers.v2
{
    /// <summary>
    /// 任务调度
    /// </summary>
    [CustomRoute(ApiVersions.V2)]
    [ApiController]
    [Authorize(Permissions.Name)]
  
 
    public class TasksQzController : ControllerBase
    {
        private readonly ITasksQzServices _tasksQzServices;
        private readonly ISchedulerCenter _schedulerCenter;
        private readonly IUnitOfWork _unitOfWork;

        public TasksQzController(ITasksQzServices tasksQzServices, ISchedulerCenter schedulerCenter,
           IUnitOfWork unitOfWork)
        {
            this._tasksQzServices = tasksQzServices;
            this._schedulerCenter = schedulerCenter;
            _unitOfWork = unitOfWork;
        }


        /// <summary>
        ///查看任务列表 分页获取
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetPage")]
        public async Task<MessageModel<PageModels<TasksQz>>> GetPage(int pageIndex, int PageSize)
        {
          
            var data = await _tasksQzServices.GetPage(pageIndex, PageSize);
            return new MessageModel<PageModels<TasksQz>>()
            {
                msg = "获取成功",
                success = true,
                response = new PageModels<TasksQz>()
                {
                    page = pageIndex,
                    PageSize = PageSize,
                    dataCount = data.dataCount,
                    data = data.data,
                    pageCount = data.pageCount,
                }
            };
        }
        /// <summary>
        /// 添加定时任务
        /// </summary>
        /// <param name="tasksQz"></param>
        /// <returns></returns>
        [HttpPost("TasksQzAdd")]
        public async Task<MessageModel<string>> TasksQzAdd(TasksQz tasksQz)
        {
            var data = new MessageModel<string>();
            _unitOfWork.BeginTran();
            var id = (await _tasksQzServices.TasksQzAdd(tasksQz));
            data.success = id > 0;
            try
            {
                if (data.success)
                {
                    tasksQz.Id = id;
                    data.response = id.ObjToString();
                    data.msg = "添加成功";
                    if (tasksQz.IsStart)
                    {
                        //如果是启动自动
                        var ResuleModel = await _schedulerCenter.AddScheduleJobAsync(tasksQz);
                        data.success = ResuleModel.success;
                        if (ResuleModel.success)
                        {
                            data.msg = $"{data.msg}=>启动成功=>{ResuleModel.msg}";
                        }
                        else
                        {
                            data.msg = $"{data.msg}=>启动失败=>{ResuleModel.msg}";
                        }
                    }
                }
                else
                {
                    data.msg = "添加失败";

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (data.success)
                    _unitOfWork.CommitTran();
                else
                    _unitOfWork.RollbackTran();
            }
            return data;
        }
        /// <summary>
        /// 修改定时任务
        /// </summary>
        /// <param name="tasksQz"></param>
        /// <returns></returns>
        [HttpPut("TasksQzUpdate")]
        public async Task<MessageModel<string>> TasksQzUpdate(TasksQz tasksQz)
        {
            var data = new MessageModel<string>();
            if (tasksQz != null && tasksQz.Id > 0)
            {
                _unitOfWork.BeginTran();
                data.success = await _tasksQzServices.TasksQzUpdate(tasksQz);
                try
                {
                    if (data.success)
                    {
                        data.msg = "修改成功";
                        data.response = tasksQz?.Id.ObjToString();
                        if (tasksQz.IsStart)
                        {
                            var ResuleModelStop = await _schedulerCenter.StopScheduleJobAsync(tasksQz);
                            data.msg = $"{data.msg}=>停止:{ResuleModelStop.msg}";
                            var ResuleModelStar = await _schedulerCenter.AddScheduleJobAsync(tasksQz);
                            data.success = ResuleModelStar.success;
                            data.msg = $"{data.msg}=>启动:{ResuleModelStar.msg}";
                        }
                        else
                        {
                            var ResuleModelStop = await _schedulerCenter.StopScheduleJobAsync(tasksQz);
                            data.msg = $"{data.msg}=>停止:{ResuleModelStop.msg}";
                        }
                    }
                    else
                    {
                        data.msg = "修改失败";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (data.success)
                        _unitOfWork.CommitTran();
                    else
                        _unitOfWork.RollbackTran();
                }
            }
            return data;
        }
        /// <summary>
        /// 启动定时任务
        /// </summary>
        /// <returns></returns>
        [HttpGet("TasksQzStart")]
        public async Task<MessageModel<string>> TasksQzStart()
        {
            return await _schedulerCenter.StartScheduleAsync();
        }
        /// <summary>
        /// 停止定时任务
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpGet("TasksQzStop")]
        public async Task<MessageModel<string>> TasksQzStop(int jobId)
        {
            var data = new MessageModel<string>();
            var model = await _tasksQzServices.QueryById(jobId);
            if (model != null)
            {
                model.IsStart = false;
                data.success = await _tasksQzServices.TasksQzUpdate(model);
                data.response = jobId.ObjToString();
                if (data.success)
                {
                    data.msg = "更新成功";
                    var ResuleModel = await _schedulerCenter.StopScheduleJobAsync(model);
                    if (ResuleModel.success)
                    {
                        data.msg = $"{data.msg}=>停止成功=>{ResuleModel.msg}";
                    }
                    else
                    {
                        data.msg = $"{data.msg}=>停止失败=>{ResuleModel.msg}";
                    }
                }
                else
                {
                    data.msg = "更新失败";
                }
            }
            else
            {
                data.msg = "任务不存在";
            }
            return data;
        }
        /// <summary>
        /// 重启一个计划任务
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpGet("ReCovery")]
        public async Task<MessageModel<string>> ReCovery(int jobId)
        {
            var data = new MessageModel<string>();
            var model = await _tasksQzServices.QueryById(jobId);
            if (model != null)
            {

                _unitOfWork.BeginTran();
                try
                {
                    model.IsStart = true;
                    data.success = await _tasksQzServices.Update(model);
                    data.response = jobId.ObjToString();
                    if (data.success)
                    {
                        data.msg = "更新成功";
                        var ResuleModelStop = await _schedulerCenter.StopScheduleJobAsync(model);
                        var ResuleModelStar = await _schedulerCenter.AddScheduleJobAsync(model);
                        if (ResuleModelStar.success)
                        {
                            data.msg = $"{data.msg}=>停止:{ResuleModelStop.msg}=>启动:{ResuleModelStar.msg}";
                            data.response = jobId.ObjToString();

                        }
                        else
                        {
                            data.msg = $"{data.msg}=>停止:{ResuleModelStop.msg}=>启动:{ResuleModelStar.msg}";
                            data.response = jobId.ObjToString();
                        }
                        data.success = ResuleModelStar.success;
                    }
                    else
                    {
                        data.msg = "更新失败";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (data.success)
                        _unitOfWork.CommitTran();
                    else
                        _unitOfWork.RollbackTran();
                }
            }
            else
            {
                data.msg = "任务不存在";
            }
            return data;

        }
        /// <summary>
        /// 暂停一个计划任务
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>        
        [HttpGet("PauseJob")]
        public async Task<MessageModel<string>> PauseJob(int jobId)
        {
            var data = new MessageModel<string>();
            var model = await _tasksQzServices.QueryById(jobId);
            if (model != null)
            {
                _unitOfWork.BeginTran();
                try
                {
                    data.success = await _tasksQzServices.TasksQzUpdate(model);
                    data.response = jobId.ObjToString();
                    if (data.success)
                    {
                        data.msg = "更新成功";
                        var ResuleModel = await _schedulerCenter.PauseJob(model);
                        if (ResuleModel.success)
                        {
                            data.msg = $"{data.msg}=>暂停成功=>{ResuleModel.msg}";
                        }
                        else
                        {
                            data.msg = $"{data.msg}=>暂停失败=>{ResuleModel.msg}";
                        }
                        data.success = ResuleModel.success;
                    }
                    else
                    {
                        data.msg = "更新失败";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (data.success)
                        _unitOfWork.CommitTran();
                    else
                        _unitOfWork.RollbackTran();
                }
            }
            else
            {
                data.msg = "任务不存在";
            }
            return data;
        }
        /// <summary>
        /// 恢复一个计划任务
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>        
        [HttpGet("ResumeJob")]
        public async Task<MessageModel<string>> ResumeJob(int jobId)
        {
            var data = new MessageModel<string>();

            var model = await _tasksQzServices.QueryById(jobId);
            if (model != null)
            {
                _unitOfWork.BeginTran();
                try
                {
                    model.IsStart = true;
                    data.success = await _tasksQzServices.TasksQzUpdate(model);
                    data.response = jobId.ObjToString();
                    if (data.success)
                    {
                        data.msg = "更新成功";
                        var ResuleModel = await _schedulerCenter.ResumeJob(model);
                        if (ResuleModel.success)
                        {
                            data.msg = $"{data.msg}=>恢复成功=>{ResuleModel.msg}";
                        }
                        else
                        {
                            data.msg = $"{data.msg}=>恢复失败=>{ResuleModel.msg}";
                        }
                        data.success = ResuleModel.success;
                    }
                    else
                    {
                        data.msg = "更新失败";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (data.success)
                        _unitOfWork.CommitTran();
                    else
                        _unitOfWork.RollbackTran();
                }
            }
            else
            {
                data.msg = "任务不存在";
            }
            return data;
        }
        /// <summary>
        /// 删除一个任务
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public async Task<MessageModel<string>> Delete(int jobId)
        {
            var data = new MessageModel<string>();
            var model = await _tasksQzServices.QueryById(jobId);
            if (model != null)
            {
                _unitOfWork.BeginTran();
                data.success = await _tasksQzServices.Delete(model);
                try
                {
                    data.response = jobId.ObjToString();
                    if (data.success)
                    {
                        data.msg = "删除成功";
                        var ResuleModel = await _schedulerCenter.StopScheduleJobAsync(model);
                        data.msg = $"{data.msg}=>任务状态=>{ResuleModel.msg}";
                    }
                    else
                    {
                        data.msg = "删除失败";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (data.success)
                        _unitOfWork.CommitTran();
                    else
                        _unitOfWork.RollbackTran();
                }
            }
            else
            {
                data.msg = "任务不存在";
            }
            return data;

        }
    }
}
