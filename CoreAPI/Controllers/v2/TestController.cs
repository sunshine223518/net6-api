﻿using CoreAPI.Core;
using CoreAPI.EventBus.EventBus;
using CoreAPI.Extensions.EventHanding;
using CoreAPI.Extensions.Redis;
using CoreAPI.Filter;
using CoreAPI.IService;
using CoreAPI.Model.BASE;
using CoreAPI.Model.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static CoreAPI.ServiceExtensions.CustomApiVersion;

namespace CoreAPI.Controllers.v2
{ 
    /// <summary>
    /// 测试
    /// </summary>
    [CustomRoute(ApiVersions.V2)]
    [ApiController]
    //[Authorize(Permissions.Name)]
    //[Authorize]  
    public class TestController : ControllerBase
    {
        private readonly ITasksQzServices tasksQzServices;
        private readonly IRedisBasketRepository _cache;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Itest itest;

        public TestController(
            ITasksQzServices _tasksQzServices, 
            IRedisBasketRepository cache, 
            IHttpContextAccessor httpContextAccessor,
            Itest itest
            )
        {
            tasksQzServices = _tasksQzServices;
            _cache = cache;
            _httpContextAccessor = httpContextAccessor;
            this.itest = itest;
        }
        /// <summary>
        /// 测试Redis消息队列
        /// </summary>
        /// <param name="_redisBasketRepository"></param>
        /// <returns></returns>
        [HttpGet("RedisMq")]
        public async Task RedisMq([FromServices] IRedisBasketRepository _redisBasketRepository)
        {
            var msg = $"这里是一条日志{DateTime.Now}";
            await _redisBasketRepository.ListLeftPushAsync(RedisMqKey.Loging, msg);
        }
        /// <summary>
        /// 测试 post 一个对象 + 独立参数
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        [HttpGet("GetPage")]
        public async Task<MessageModel<PageModels<TasksQz>>> GetPage([FromForm] TasksQz tasks,object c)
        {
            int pageIndex = 1;
            int PageSize = 1;
            var data = await tasksQzServices.GetPage(pageIndex, PageSize);
            return new MessageModel<PageModels<TasksQz>>()
            {
                msg = "获取成功",
                success = true,
                response = new PageModels<TasksQz>()
                {
                    page = pageIndex,
                    PageSize = PageSize,
                    dataCount = data.dataCount,
                    data = data.data,
                    pageCount = data.pageCount,
                }
            };
        }
        /// <summary>
        /// 测试多库
        /// </summary>
        /// <returns></returns>
        [HttpPost("CheckDbs")]
        public async Task<object> CheckDbs()
        {
           return await itest.Query();
            
        }
        /// <summary>
        ///  测试RabbitMQ事件总线
        /// </summary>
        /// <param name="_eventBus"></param>
        /// <param name="ID"></param>
        [HttpGet]
        public void EventBusTry([FromServices] IEventBus _eventBus, string ID = "3")
        {
            var quartzJobReCoveryIntegrationEvent = new QuartzJobReCoveryIntegrationEvent(ID);
            _eventBus.Publish(quartzJobReCoveryIntegrationEvent);
        }
        [HttpPost("CheckDbs2")]
        public async Task<object> CheckDbs2([FromBody] List<Test> test)
        {
            var ccc = test;
            Console.WriteLine(test);
            return await itest.Query();

        }
        public class Test
        {
            public int id { get; set; }
            public string name { get; set; }
        }
    }
}
