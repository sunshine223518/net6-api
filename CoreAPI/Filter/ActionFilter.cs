﻿using CoreAPI.Common.Appseting;
using CoreAPI.Common.Helper;
using CoreAPI.Common.HttpContextUser;
using CoreAPI.Core;
using CoreAPI.DTO;
using CoreAPI.Extensions.Authorizations.Policys;
using CoreAPI.Extensions.Redis;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace CoreAPI.Filter
{
    /// <summary>
    /// 单点登录 每次请求都会经过这里
    /// </summary>
    public class ActionFilter : IAsyncActionFilter
    {
        private readonly IRedisBasketRepository _redis;
        private readonly IUser _user;
        private readonly PermissionRequirement requirement;
        private readonly IHttpContextAccessor _accessor;

        public ActionFilter(IRedisBasketRepository redis, IUser user, PermissionRequirement requirement, IHttpContextAccessor accessor)
        {
            this._redis = redis;
            _user = user;
            _accessor = accessor;
            this.requirement = requirement;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            //如果为jwt 功能作废 因为登录接口写入了单点详情见 api/v2/login/gettoken
            if (Permissions.IsUseIds4)
            {
                if (Appsettings.app("Middleware", "SinglePoint", "Enabled").ObjToBool())
                {
                    string UserName = string.Empty;
                    var httpContext = _accessor.HttpContext;
                    var UserNames = (from item in httpContext.User.Claims
                                     where item.Type == "name"
                                     select item.Value).ToList();
                    if (UserNames.Count > 0)
                    {
                        UserName = UserNames[0].ToString();
                    }
                    var IsFlag = await _redis.Exist(UserName);
                    if (IsFlag)
                    {
                        //var RedisCode1 = await _redis.Get<UserInfoSinilgePoints>(UserName);

                        ////判断是否只能允许一个账号在一个终端只能登录一次 
                        //if (Appsettings.app("Middlew0are", "SinglePoints", "Enabled").ObjToBool())
                        //{
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        await next();
                        return;
                        //}
                        //else
                        //{
                        //   //需要该功能就去掉注释
                        //    //获取机器码   
                        //    var OnlyCode = MachineCodeHelper.GetOnly();
                        //    var RedisCode = await _redis.Get<UserInfoSinilgePoints>(UserName);
                        //    //防止redis断开连接
                        //    if (RedisCode == null)
                        //    {
                        //        Thread.Sleep(20);
                        //        RedisCode = await _redis.Get<UserInfoSinilgePoints>(UserName);
                        //    }
                        //    if (OnlyCode != RedisCode.OnlyCode)
                        //    {
                        //       
                        //        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        //        return;
                        //    }
                        //}
                    }
                    else
                    {
                        await next();
                    }
                }
                else
                    await next();
            }
            else
                await next();
        }
    }
}
