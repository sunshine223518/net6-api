using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using CoreAPI.ServiceExtensions;
using CoreAPI.Extensions.ServiceExtensions;
using CoreAPI.Extensions;
using CoreAPI.Core;
using Microsoft.AspNetCore.Http;
using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using CoreAPI.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using CoreAPI.Common.Appseting;
using CoreAPI.Common.Helper;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Text;
using Microsoft.Extensions.Hosting;
using CoreAPI.Extensions.Middlewares;
using CoreAPI.Extensions.Extensions;
using CoreAPI.Model.Seed;
using CoreAPI.IService;
using CoreAPI.Tasks;
using CoreAPI.Common;
using System.Reflection;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;

//.NET6 写法
var builder = WebApplication.CreateBuilder(args);
//取消上传文件大小限制
builder.WebHost.ConfigureKestrel(serverOptions =>
{
    serverOptions.Limits.MaxRequestBodySize = null;
});
// 1、配置host与容器
builder.Host
.UseServiceProviderFactory(new AutofacServiceProviderFactory())
.ConfigureContainer<ContainerBuilder>(builder =>
{
    builder.RegisterModule(new AutofacModuleRegister());
    builder.RegisterModule<AutofacPropertityModuleReg>();
})
.ConfigureLogging((hostingContext, builder) =>
{
    builder.AddFilter("System", LogLevel.Error);
    builder.AddFilter("Microsoft", LogLevel.Error);
    builder.SetMinimumLevel(LogLevel.Error);
    builder.AddLog4Net(Path.Combine(Directory.GetCurrentDirectory(), "Log4net.config"));
})
.ConfigureAppConfiguration((hostingContext, config) =>
{
    config.Sources.Clear();
    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
});


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

// 2、配置服务
builder.Services.AddSingleton(new Appsettings(builder.Configuration));
builder.Services.AddSingleton(new LogLock(builder.Environment.ContentRootPath));

Permissions.IsUseIds4 = Appsettings.app(new string[] { "Startup", "IdentityServer4", "Enabled" }).ObjToBool();
RoutePrefix.Name = Appsettings.app(new string[] { "AppSettings", "SvcName" }).ObjToString();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddMemoryCacheSetup();
builder.Services.AddRedisCacheSetup();

builder.Services.AddSqlsugarSetup();
builder.Services.AddDbSetup();

builder.Services.AddAutoMapperSetup();
builder.Services.AddCorsSetup();
builder.Services.AddMiniProfilerSetup();
builder.Services.AddSwaggerSetup();
builder.Services.AddJobSetup();
builder.Services.AddHttpContextSetup();
builder.Services.AddAppConfigSetup(builder.Environment);
builder.Services.AddHttpApi();
builder.Services.AddRedisInitMqSetup();
builder.Services.AddRabbitMQSetup();
builder.Services.AddEventBusSetup();//事件总览
                                    // 授权+认证 (jwt or ids4)
builder.Services.AddAuthorizationSetup();
if (Permissions.IsUseIds4)
{
    builder.Services.AddAuthentication_Ids4Setup();
}
else
{
    builder.Services.AddAuthentication_JWTSetup();
}
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

//ip限流 .net6新增
builder.Services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
builder.Services.AddIpPolicyRateLimitSetup(builder.Configuration);//对接口进行保护，IP限流
builder.Services.AddSignalR().AddNewtonsoftJsonProtocol();

builder.Services.Configure<KestrelServerOptions>(x => x.AllowSynchronousIO = true)
       .Configure<IISServerOptions>(x => x.AllowSynchronousIO = true);

builder.Services.AddControllers(o =>
{
    //全局api过滤
    o.Filters.Add(typeof(ActionFilter));
    // 全局异常过滤
    o.Filters.Add(typeof(GlobalExceptionsFilter));

    // 全局路由前缀，统一修改路由
    // 全局路由权限公约
    //o.Conventions.Insert(0, new GlobalRouteAuthorizeConvention());
    // 全局路由前缀，统一修改路由
    o.Conventions.Insert(0, new GlobalRoutePrefixFilter(new RouteAttribute(RoutePrefix.Name)));
})
//.net5后需要新增 Microsoft.AspNetCore.Mvc.NewtonsoftJson这个包 放在扩展层下
//全局配置Json序列化处理
.AddNewtonsoftJson(options =>
{
    //忽略循环引用
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    //不使用驼峰样式的key
    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
    //设置时间格式
    //options.SerializerSettings.DateFormatString = "yyyy-MM-dd";
    //忽略Model中为null的属性
    //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
});
builder.Services.AddEndpointsApiExplorer();

builder.Services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

// 3、配置中间件
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
    //app.UseHsts();
}
// Ip限流,尽量放管道外层
app.UseIpLimitMildd();
app.UseMiddleware<RealIpMiddleware>();
// 记录请求与返回数据 
app.UseReuestResponseLog();
// 用户访问记录(必须放到外层，不然如果遇到异常，会报错，因为不能返回流)
app.UseRecordAccessLogsMildd();
// signalr 
app.UseSignalRSendMildd();
// 记录ip请求
app.UseIPLogMildd();
// 查看注入的所有服务
app.UseAllServicesMildd(builder.Services);

if (builder.Environment.IsDevelopment())
{
    // 在开发环境中，使用异常页面，这样可以暴露错误堆栈信息，所以不要放在生产环境。
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
    // 在非开发环境中，使用HTTP严格安全传输(or HSTS) 对于保护web安全是非常重要的。
    // 强制实施 HTTPS 在 ASP.NET Core，配合 app.UseHttpsRedirection
    //app.UseHsts();
}
// 封装Swagger展示
app.UseSwaggerMildd(() => Assembly.GetExecutingAssembly().GetManifestResourceStream("CoreAPI.index.html"));
// ↓↓↓↓↓↓ 注意下边这些中间件的顺序，很重要 ↓↓↓↓↓↓
// CORS跨域
app.UseCors(Appsettings.app(new string[] { "Startup", "Cors", "PolicyName" }));
// 使用静态文件
app.UseStaticFiles();
// 使用cookie
app.UseCookiePolicy();
// 返回错误码
app.UseStatusCodePages();//把错误码返回前台，比如是404
app.UseRouting();
// 先开启认证
app.UseAuthentication();
// 这种自定义授权中间件，可以尝试，但不推荐
// app.UseJwtTokenAuth();
// 然后是授权中间件
app.UseAuthorization();
// 性能分析

app.UseMiniProfilerMildd();
// 开启异常中间件，要放到最后
//app.UseExceptionHandlerMidd();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
          name: "default",
          pattern: "{controller=Home}/{action=Index}/{id?}");

    endpoints.MapHub<ChatHub>("/api2/chatHub");
});
var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();
var myContext = scope.ServiceProvider.GetRequiredService<MyContext>();
var tasksQzServices = scope.ServiceProvider.GetRequiredService<ITasksQzServices>();
var schedulerCenter = scope.ServiceProvider.GetRequiredService<ISchedulerCenter>();
var lifetime = scope.ServiceProvider.GetRequiredService<IHostApplicationLifetime>();
// 初始化生成表
app.UseSeedDataMildd(myContext, builder.Environment.ContentRootPath);
// 开启QuartzNetJob调度服务
app.UseQuartzJobMildd(tasksQzServices, schedulerCenter);
//服务注册
//   app.UseConsul(Configuration);
app.UseConsulMildd(builder.Configuration, lifetime);
// 4、运行
app.Run();

