#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["CoreAPI/CoreAPI.csproj", "CoreAPI/"]
COPY ["CoreAPI.Extensions/CoreAPI.Extensions.csproj", "CoreAPI.Extensions/"]
COPY ["CoreAPI.Service/CoreAPI.Service.csproj", "CoreAPI.Service/"]
COPY ["CoreAPI.Repository/CoreAPI.Repository.csproj", "CoreAPI.Repository/"]
COPY ["CoreAPI.Common/CoreAPI.Common.csproj", "CoreAPI.Common/"]
COPY ["CoreAPI.Model/CoreAPI.Model.csproj", "CoreAPI.Model/"]
COPY ["CoreAPI.IService/CoreAPI.IService.csproj", "CoreAPI.IService/"]
COPY ["CoreAPI.DTO/CoreAPI.DTO.csproj", "CoreAPI.DTO/"]
COPY ["CoreAPI.Tasks/CoreAPI.Tasks.csproj", "CoreAPI.Tasks/"]
COPY ["CoreAPI.EventBus/CoreAPI.EventBus.csproj", "CoreAPI.EventBus/"]
RUN dotnet restore "CoreAPI/CoreAPI.csproj"
COPY . .
WORKDIR "/src/CoreAPI"
RUN dotnet build "CoreAPI.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "CoreAPI.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "CoreAPI.dll"]